package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
        System.out.println("Let's play round " + roundCounter);
        String human_choice = readInput("Your choice (Rock/Paper/Scissors)?");
        if (!human_choice.equals("paper") && !human_choice.equals("rock") && !human_choice.equals("scissors")) {
            System.out.println("I don't understand "+ human_choice+". Could you try again?");            
            }
        else {

            Random randomizer = new Random();
            String computer_choice = rpsChoices.get(randomizer.nextInt(rpsChoices.size()));
            
            if (human_choice.equals(computer_choice)) {
                System.out.println("Human chose " + human_choice + "," + " " + "computer chose " + computer_choice + "." + " It's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else if (
            (human_choice.equals("paper")) && computer_choice.equals("rock") || 
            (human_choice.equals("rock")) && computer_choice.equals("scissors") || 
            (human_choice.equals("scissors") && computer_choice.equals("paper"))) {
                // Human wins
                humanScore++;
                System.out.println("Human chose " + human_choice + ", computer chose " + computer_choice + ". Human wins!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else {
                // Computer wins
                //(human_choice.equals("rock") && computer_choice.equals("paper") || 
                //(human_choice.equals("scissors") && computer_choice.equals("rock") ||
                //human_choice.equals("paper") && computer_choice.equals("scissors") || 
                computerScore++;
                System.out.println("Human chose " + human_choice + ", computer chose " + computer_choice + ". Computer wins!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }

            roundCounter++;
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?");
            if (continue_answer.equals("y")) {
                continue;
            }
            else {                
                System.out.println("Bye bye :)");
            break;
            }




            

        }
        }

    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
